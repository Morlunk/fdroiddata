Categories:Office
License:GPLv3
Web Site:http://droidparty.net/
Source Code:https://github.com/chr15m/PdDroidParty
Issue Tracker:https://github.com/chr15m/PdDroidParty/issues

Auto Name:PdDroid Party
Summary:Run Pure Data patches
Description:
Run your Pure Data patches on Android with native GUIs emulated.
.

Repo Type:git
Repo:https://github.com/chr15m/PdDroidParty

Build:0.3.0,2
    disable=libpd, scanner
    commit=c511fc4ad324817ab80fb3e6c44a7effaaf6735d
    submodules=yes
    rm=libs/*.jar
    srclibs=2:SVGAndroid@ae2e0405fcff18d46fde59d0a3f970661813ad89,Libpd@0.7.0
    prebuild=\
        pushd pd-for-android/PdCore && \
        find libs -iname *.so | xargs rm && \
        android update project --path . && \
        ndk-build && \
        popd &&  android update project --name PdDroidParty --path .

Maintainer Notes:

TODO:libpd has jar files
TODO:libpd ndk-build doesn't work

        pushd $$Libpd$$ && cp Android.mk jni/ && ndk-build && popd &&\
.
#Auto Update Mode:Version v%v
#Update Check Mode:Tags
Update Check Mode:RepoManifest
Current Version:0.3.0
Current Version Code:2

